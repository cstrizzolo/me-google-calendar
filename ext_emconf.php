<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "me_google_calendar".
 *
 * Auto generated 08-12-2013 13:24
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['me_google_calendar'] = [
    'title' => 'Google Calendar',
    'description' => 'Includes the jQuery Plugin FullCalendar, which generates a skinable calendar with different views (month, week, day, week list, day list etc.) from Google Calendar XML Feed(s) - now with extbase/fluid.',
    'category' => 'plugin',
    'version' => '4.14.0',
    'state' => 'stable',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' => [
        'depends' =>
            [
                'typo3' => '9.5.25-11.5.99',
            ],
        'conflicts' =>
            [],
        'suggests' =>
            [],
    ],
    'autoload' => [
        'psr-4' =>
            [
                'MediaEssenz\\MeGoogleCalendar\\' => 'Classes',
            ],
    ],
];
