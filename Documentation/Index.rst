﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

﻿.. include:: ./Includes.txt

EXT: Google Calendar
====================

:Extensionkey:
      me_google_calendar

:Keywords:
      Google, Calendar, FullCalendar, jQuery

:Languages:
      en, de

:Created:
      2010-01-07

:Last Changed:
      2021-05-24

:Author:
      Alexander Grein

:Email:
      alexander.grein@gmail.com

Table of Content
^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   Introduction/Index
   UsersManual/Index
   Configuration/Index
   KnownProblems/Index
   Todos/Index
   Changelog/Index

