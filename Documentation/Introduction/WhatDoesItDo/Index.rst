﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

What does it do?
^^^^^^^^^^^^^^^^

- This extension includes the jQuery Plugin `FullCalendar
  <http://fullcalendar.io/>`_ , which generates a skinable
  Calendar with different views (month, week, day, week list, day list)
  from Google Calendar XML Feed(s).

- It uses Extbase and Fluid and needs at least TYPO3 8.7.8 (<= v4.9.1) or 9.5.0 (>= v4.10.0)

- The extension is localized in english and german. Contact me, if you
  would like to add your language.

- For examples of the different views visit `http://fullcalendar.io
  <http://fullcalendar.io/>`_
